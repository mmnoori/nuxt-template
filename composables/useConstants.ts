export const useConstants = () => {
  return {
    DIALOGS: {
      THEME_DIALOG: 'DialogTheme',
      LANG_DIALOG: 'DialogLangFont',
    },
    STORAGE_KEYS: {
      LANG: 'Lang',
      THEME: 'Theme',
    }
    // Return other constants here
  }
}
