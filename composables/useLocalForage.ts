// useLocalForage.ts
import { ref } from 'vue'
import localforage from 'localforage'

export function useLocalForage(storeName: string = 'defaultStore') {
  const storage = localforage.createInstance({
    name: 'myApp',
    storeName: storeName,
    // driver: localforage.LOCALSTORAGE
  })

  const setItem = async <T>(key: string, value: T): Promise<void> => {
    try {
      await storage.setItem(key, value)
    } catch (error) {
      console.error(`Error setting item ${key} to localforage`, error)
    }
  }

  const getItem = async <T>(key: string): Promise<T | null> => {
    try {
      return await storage.getItem<T>(key)
    } catch (error) {
      console.error(`Error getting item ${key} from localforage`, error)
      return null
    }
  }

  const removeItem = async (key: string): Promise<void> => {
    try {
      await storage.removeItem(key)
    } catch (error) {
      console.error(`Error removing item ${key} from localforage`, error)
    }
  }

  const clear = async (): Promise<void> => {
    try {
      await storage.clear()
    } catch (error) {
      console.error('Error clearing localforage', error)
    }
  }

  const length = async (): Promise<number> => {
    try {
      return await storage.length()
    } catch (error) {
      console.error('Error getting length of localforage', error)
      return 0
    }
  }

  const keys = async (): Promise<string[]> => {
    try {
      return await storage.keys()
    } catch (error) {
      console.error('Error getting keys from localforage', error)
      return []
    }
  }

  const iterate = async <T>(
    iteratee: (value: T, key: string, iterationNumber: number) => any
  ): Promise<void> => {
    try {
      await storage.iterate(iteratee)
    } catch (error) {
      console.error('Error iterating over localforage', error)
    }
  }

  return {
    setItem,
    getItem,
    removeItem,
    clear,
    length,
    keys,
    iterate,
  }
}
