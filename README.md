
## Development

#### Development Environment

- Docker
  - docker run --rm -d -it -p 5555:5555 -p 6060:6060  -v .:/app -w /app --name develop-fara node:20.14-alpine

#### Development Prerequisites
- VS Code
- Extensions 
  - Vue Official 
  - Sass 
  - ESLint 
  - Prettier

```bash
# Start Develop Server
npm run dev
```
#### Localization Guides
[How to Handle Localization?](docs/localization.md)

#### Styling Guides
[How to Style Project?](docs/styling.md)


## Production (CI/CD)
```bash
# Install Packages
npm install
```

```bash
# Build the application for production:
npm run build
```

```bash
# Locally preview production build:
npm run preview
```

Check out the [deployment documentation](https://nuxt.com/docs/getting-started/deployment) for more information.
