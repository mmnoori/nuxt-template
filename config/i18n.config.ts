// Vuetify Translations
import { en as english, fa as farsi } from 'vuetify/locale'

// Custom Translations
import faMessages from './messages/fa.js'
import enMessages from './messages/en.js'

export default defineI18nConfig(() => ({
  legacy: false,
  locale: 'fa',
  messages: {
    en: {
      ...enMessages,
      $vuetify: english
    },
    fa: {
      ...faMessages,
      $vuetify: farsi
    }
  }
}))
