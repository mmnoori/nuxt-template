export default {
  navigation: {
    setting: 'تنظیمات',
    home: 'صفحه اصلی',
    favorite: 'مورد علاقه',
    assets: 'دارایی',

  },
  setting: {
    category: {
      appearance: 'تنظیمات ظاهری',
      technicalRisk: 'ریسک فنی',
      backupRestore: 'پشتیبان گیری و بازگردانی',
    },
    title: {
      theme: 'تم',
      languageAndFont: 'زبان و فونت',
    },
    desc: {
      theme: 'تم برنامه را شخصی سازی کنید',
      languageAndFont: 'تغییر زبان و فونت برنامه',
    },
  },
  modal: {
    theme: {
      title: 'تم مورد نظر را انتخاب کنید'
    },
    lang: {
      title: 'زبان مورد نظر را انتخاب کنید'
    },
  },
  loading: {
    default: 'لطفا صبر کنید ...',
  },
  button: {
    login: 'ورود',
    logout: 'خروج'

  },

  input: {
    validation: {
       minLength: '{field} حداقل باید {length} کاراکتر باشد',
       maxLength: '{field} باید کمتر از {length} کاراکتر باشد',
       noDigitAllowed: '{field} نمیتواند شامل عدد باشد',

    },
    field: {
      username: 'نام کاربری',
      password: 'رمز عبور',
    }
  },
  routing: {
    notFound: 'صفحه مورد نظر پیدا نشد'
  }
}
