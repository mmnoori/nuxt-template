
export default {
  navigation: {
    setting: 'Setting',
    home: 'Home',
    favorite: 'ّFavorite',
    assets: 'Assets',
  },
  setting: {
    category: {
      appearance: 'Appearance',
      technicalRisk: 'Techncal Risk',
      backupRestore: 'Backup And Restore',
    },
    title: {
      theme: 'Theme',
      languageAndFont: 'Language & Font',
    },
    desc: {
      theme: 'Customize your Theme',
      languageAndFont: 'Customize Application Language & Typography',
    },

  },
  modal: {
    theme: {
      title: 'Select a theme'
    },
    lang: {
      title: 'Select a Language'
    },
  },
  loading: {
    default: 'Please Wait...',
  },
  button: {
    login: 'Login',
    logout: 'Logout'
  },

  input: {
    validation: {
      minLength: '{field} must be at least {length} characters',
      maxLength: '{field} must be less than {length} characters',
      noDigitAllowed: '{field} must Not contain digits',
      // minLength: 'Must be less than {name} {lastName} characters',
    },
    field: {
      username: 'Username',
      password: 'Password',
    }
  },
  routing: {
    notFound: 'Page Not Found'
  }
}
