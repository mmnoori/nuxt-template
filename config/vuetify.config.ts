// vuetify.config.ts
import { defineVuetifyConfiguration } from 'vuetify-nuxt-module/custom-configuration'

// Translations provided by Vuetify
// import { pl, zhHans } from 'vuetify/locale'
import * as locales from 'vuetify/locale'

import colors from 'vuetify/util/colors'

const lightTheme = {
  dark: false,
  colors: {
    primary: colors.red.darken1, // #E53935
    secondary: colors.red.lighten4, // #FFCDD2
  },
}

const myCustomLightTheme = {
  dark: false,
  colors: {
    primary: '#424242',
    // secondary: colors.purple-lighten-1,

    // background: '#FFFFFF',
    // surface: '#FFFFFF',
    // 'surface-bright': '#FFFFFF',
    // 'surface-light': '#EEEEEE',
    // 'surface-variant': '#424242',
    // 'on-surface-variant': '#EEEEEE',
    // primary: '#D90166',
    // 'primary-darken-1': '#1F5592',
    // secondary: '#D90166',
    // 'secondary-darken-1': '#018786',
    // error: '#B00020',
    // info: '#2196F3',
    // success: '#4CAF50',
    // warning: '#FB8C00',
  },
  variables: {
    'border-color': 'red',
    'border-opacity': 0.12,
    'high-emphasis-opacity': 0.87,
    'medium-emphasis-opacity': 0.6,
    'disabled-opacity': 0.38,
    'idle-opacity': 0.04,
    'hover-opacity': 0.04,
    'focus-opacity': 0.12,
    'selected-opacity': 0.08,
    'activated-opacity': 0.12,
    'pressed-opacity': 0.12,
    'dragged-opacity': 0.08,
    'theme-kbd': '#212529',
    'theme-on-kbd': '#FFFFFF',
    'theme-code': '#F5F5F5',
    'theme-on-code': '#000000',

    'bottom-navigation-button-color': 'red',
    'button-color': 'yellow',
  },
}

export default defineVuetifyConfiguration({
  /* vuetify options */
  theme: {
    // THEME
    // defaultTheme: 'dark',
    // defaultTheme: 'myCustomLightTheme',
    defaultTheme: 'lightTheme',
    themes: {
      myCustomLightTheme,
      lightTheme,
    },
  },
  // labComponents: ['VTreeview'],
  labComponents: true
})
