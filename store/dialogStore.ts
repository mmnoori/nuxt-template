// ~/store/counterStore.js
import { defineStore } from 'pinia'

interface DialogProps {
  [key: string]: any // Allows for any string key with any type of value
}

// Define the state type
interface DialogState {
  isOpen: boolean
  dialogName: string
  dialogTitle: string
  props: DialogProps
}

export const useDialogStore = defineStore<'dialog', DialogState>({
  id: 'dialog',
  state: (): DialogState => ({
    isOpen: false,
    dialogName: '',
    dialogTitle: '',
    props: {},
  }),
  getters: {
    // doubleCount: (state) => state.count * 2,
  },
  actions: {
    // openDialog(dialogName: string, asd: string) {
    openDialog(dialogName: string, options: { title: string }) {
      this.isOpen = true
      this.dialogName = dialogName
      this.dialogTitle = options.title
    },
    closeDialog() {
      this.isOpen = false
      this.dialogName = ''
    },
  },
})
