// ~/store/counterStore.js
import { defineStore } from 'pinia'

// Define the state type
interface DialogState {
  isLoading: boolean
  message: string
}

export const useLoaderStore = defineStore<
  'loader', /// ID
  DialogState, // State Type
  Record<string, never>, // Getter Types
  {
    // Action Types
    startLoader: () => void
    closeloader: () => void
  }
>({
  id: 'loader',
  state: (): DialogState => ({
    isLoading: false,
    message: '',
  }),
  actions: {
    startLoader() {
      this.isLoading = true
    },
    closeloader() {
      this.isLoading = false
    },
  },
})
