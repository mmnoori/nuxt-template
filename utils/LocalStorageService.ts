import localforage from 'localforage'

class LocalStorageService {
  private storage: LocalForage

  constructor(storeName: string = 'defaultStore') {
    this.storage = localforage.createInstance({
      name: 'myApp',
      storeName: storeName,
    })
  }

  async setItem<T>(key: string, value: T): Promise<void> {
    try {
      await this.storage.setItem(key, value)
    } catch (error) {
      console.error(`Error setting item ${key} to localforage`, error)
    }
  }

  async getItem<T>(key: string): Promise<T | null> {
    try {
      return await this.storage.getItem<T>(key)
    } catch (error) {
      console.error(`Error getting item ${key} from localforage`, error)
      return null
    }
  }

  async removeItem(key: string): Promise<void> {
    try {
      await this.storage.removeItem(key)
    } catch (error) {
      console.error(`Error removing item ${key} from localforage`, error)
    }
  }

  async clear(): Promise<void> {
    try {
      await this.storage.clear()
    } catch (error) {
      console.error('Error clearing localforage', error)
    }
  }

  async length(): Promise<number> {
    try {
      return await this.storage.length()
    } catch (error) {
      console.error('Error getting length of localforage', error)
      return 0
    }
  }

  async keys(): Promise<string[]> {
    try {
      return await this.storage.keys()
    } catch (error) {
      console.error('Error getting keys from localforage', error)
      return []
    }
  }

  async iterate<T>(
    iteratee: (value: T, key: string, iterationNumber: number) => any
  ): Promise<void> {
    try {
      await this.storage.iterate(iteratee)
    } catch (error) {
      console.error('Error iterating over localforage', error)
    }
  }
}

export default LocalStorageService
