export default [
  {
    title: 'Category 1',
    id: '4389',
    icon: 'mdi-account-box-multiple',
 
    children: [
      {
        title: 'Sub-Category-1-1',
        id: '456',
        icon: 'mdi-update',
        children: [
          {
            title: 'sub-sub-Category-1',
            id: '89o8',
          },
          {
            title: 'sub-sub-Category-2',
            id: '56743',
            children: [
              {
                title: 'sub-sub-sub-Category-1-2',
                id: '7965',
              },
            ],
          },
        ],
      },
      {
        title: 'Sub-Category-1-2',
        id: '789465',
        icon: 'mdi-adjust',
      },
    ],
  },
  {
    title: 'Category 2',
    id: '890',
    icon: 'mdi-alpha',
  },
  {
    title: 'Category 3',
    id: '5656',
    icon: 'mdi-ammunition',

    children: [
      {
        title: 'Sub-Category-3-1',
        id: '2134',
      },
      {
        title: 'Sub-Category-3-2',
        id: '345345',
      },
    ],
  },
]
