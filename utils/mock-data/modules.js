export default [
  {
    title: 'Technical Risk',
    id: '4389',
    icon: 'mdi-account-box-multiple',
 
    children: [
      {
        title: 'Hosts',
        id: '456',
        icon: 'mdi-update',
      },
      {
        title: 'Vulnerabilities',
        id: '789465',
        icon: 'mdi-adjust',
      },
      {
        title: 'Risks',
        id: '789465',
        icon: 'mdi-adjust',
      },
    ],
  },
  {
    title: 'Governance',
    id: '890',
    icon: 'mdi-alpha',
  },
  {
    title: 'Software',
    id: '5656',
    icon: 'mdi-ammunition',

    children: [
      {
        title: 'Operating Systems',
        id: '2134',
      },
      {
        title: 'Applications',
        id: '345345',
      },
    ],
  },
]
