import withNuxt from './.nuxt/eslint.config.mjs'

export default withNuxt(
  // your custom flat configs go here, for example:
  // {
  //   files: ['**/*.ts', '**/*.tsx'],
  //   rules: {
  //     'no-console': 'off' // allow console.log in TypeScript files
  //   }
  // },
  // {
  //   ...
  // }
  {
    rules: {
      // 'vue/max-attributes-per-line': [
      //   'error',
      //   {
      //     singleline: 3,
      //     multiline: {
      //       max: 1,
      //       allowFirstLine: false,
      //     },
      //   },
      // ],

      // Allow unused vars
      'no-unused-vars': 'true',
      // Do Not allow semicolons
      semi: [2, 'never'],
      // Ignore single word components
      'vue/multi-word-component-names': 'off',
      // Allow self closing elements
      'vue/html-self-closing': 'off',
    },
  }
)
