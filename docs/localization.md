### Do we need vue-i18n?
- The Vuetify locale service only provides a basic translation function t, and should really only be used for internal or custom Vuetify components. 
- Declarations can be found at node_modules/vuetify/lib/locale.

## Usage
### How to use i18n translations in template:

```html
   <h1>{{ $t('welcome') }}</h1>
```

### How to use i18n translations in script:

```js
<script setup>
  const { t } = useI18n()
  t('welcome')
</script>
```

### How to access vuetify translations:

```js
<script setup>
  const locale = useLocale()
  locale.$t('$vuetify.open')
</script>
```



