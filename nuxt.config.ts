// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  ssr: false,
  devtools: { enabled: true },

  devServer: {
    port: 5555,
  },

  // To enable type-checking at build time
  // typescript: {
  //   typeCheck: true,
  // strict: false
  // }

  vite: {
    css: {
      preprocessorOptions: {
        sass: {
          // Using this we can access classes and variables in SFC Styling
          // additionalData: '@use "~/assets/sass/_colors.sass" as *\n',
          // additionalData: '@use "~/assets/sass/main.sass"',
        },
      },
    },
  },

  modules: [
    '@nuxt/eslint',
    'vuetify-nuxt-module',
    '@pinia/nuxt',
    '@nuxtjs/i18n',
  ],

  eslint: {
    // options here
  },

  i18n: {
    // Module Options

    // if not using RTL, you can replace locales with codes only
    // locales: ['en', 'es'],
    locales: [
      {
        code: 'en',
        name: 'English',
      },
      {
        code: 'fa',
        name: 'فارسی',
        dir: 'rtl',
      },
    ],
    defaultLocale: 'en',
    strategy: 'no_prefix', // or 'prefix_except_default'

    vueI18n: './config/i18n.config.ts', // if you are using custom path, default
  },

  vuetify: {
    moduleOptions: {
      /* module specific options */
      styles: { configFile: './assets/sass/vars/settings.sass' },
    },
    vuetifyOptions: './config/vuetify.config.ts',
  },

  compatibilityDate: '2024-07-06',
})